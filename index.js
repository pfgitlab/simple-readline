/*global Promise*/

const readline = require("readline");

const rl = readline.createInterface({
  "input": process.stdin,
  "output": process.stdout
});

rl.pause();

let handlers = [];

rl.on("line", input =>
  handlers[0] &&  handlers.shift()(input)
);

module.exports = () => {
  return new Promise((resolve, reject) => {
    rl.resume(); 
    handlers.push((input) => {
      resolve(input);
      if(handlers.length <= 0) rl.pause();
    });
  });
};
