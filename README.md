# csimple-readline

A simple module to use readline in node

## Example

Basic Usage

```javascript
await readline() // returns a string
```

Echo input

```javascript
const readline = require("simple-readline");

(async() => {
  while(true) {
    console.log(await readline());
  }
})();
```

If two readlines are called at the same time, the one called first will be returned first
